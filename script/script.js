/* Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?
Можна визначити, яку саме клавішу клавіатури натиснув користувач за допомогою обробника подій keydown.
document.addEventListener('keydown', function(event) {...});

2. Яка різниця між event.code() та event.key()?
event.key() - показує який символ був натиснутий, це значення змінюється в залежності від мови.
event.code() - показує фізичний код клавіші, це значення завжди залишається тим самим.

3. Які три події клавіатури існує та яка між ними відмінність?
keydown - коли натиснули на клавішу
keyup - коли відтиснули клавішу
keypress - коли натиснули на клавішу, показує, що символ був введений.

Практичне завдання.
Реалізувати функцію підсвічування клавіш.*/

document.addEventListener('keydown', function (event) {
    const btn = document.querySelectorAll('.btn');
    const activeButton = event.key.toUpperCase();
    btn.forEach(function(button) {
        if (button.textContent.toUpperCase() === activeButton) {
            button.style.backgroundColor = 'blue';
        } else {
            button.style.backgroundColor = 'black';
        }
    })
})

